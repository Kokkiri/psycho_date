import React from 'react'
import { Link } from 'react-router-dom'

const Header = () => {
    const style = {
        nav: {
            width: '100%',
            height: '100px',
            backgroundColor: 'rgba(0, 0, 0, .2)',
        },
        link: {
            textDecoration: 'none',
            color: 'white'
        },
        ul: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '50px',
            transform: 'translateY(-50%)'
        },
        space: {
            width: '100px'
        }
    }

    return (
        <nav style={style.nav} >
            <ul style={style.ul} >
                <li>
                    <Link to="/" style={style.link}>Home</Link>
                </li>
                <li>
                    <div style={style.space} />
                </li>
                <li>
                    <Link to="/avatars" style={style.link}>Avatars</Link>
                </li>
            </ul>
        </nav>
    )
}

export { Header }