import { area } from '../utils/calc'
import { set_style_psy_model } from '../css/style'
import { point_init, stat_init } from '../utils/data'


const Psy_model = ({ stat=stat_init, point=point_init, is_active=0 }) => {

    const set_css_value = () => {
        let copy_obj = {...stat}
        copy_obj.area_1 = area(stat.area_1 + point['1'])
        copy_obj.area_2 = area(stat.area_1 + stat.area_2 + point['2'])
        copy_obj.area_3 = area(stat.area_1 + stat.area_2 + stat.area_3 + point['3'])
        copy_obj.area_4 = area(stat.area_1 + stat.area_2 + stat.area_3 + stat.area_4 + point['4'])
        return copy_obj
    }

    const style = set_style_psy_model(set_css_value())

    return (
        <div style={style.container} >
            <div style={style.div_4} className={is_active == 4 ? 'active' : ''} >
                <div style={style.div_3} className={is_active == 3 ? 'active' : ''} >
                    <div style={style.div_2} className={is_active == 2 ? 'active' : ''} >
                        <div style={style.div_1} className={is_active == 1 ? 'active' : ''} ></div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { Psy_model }