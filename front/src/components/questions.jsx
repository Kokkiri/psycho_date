import { useState } from 'react'
import { Psy_model } from "./psy_model"
import { questions, stat_init, point_init } from '../utils/data'


const Questions = () => {
    const [indexQuestion, setIndexQuestion] = useState(0)
    const [point, setPoint] = useState(point_init)
    const [stat, setStat] = useState(stat_init)

    const next_question = () => {
        let copy_indexQuestion = indexQuestion
        copy_indexQuestion = (copy_indexQuestion + 1) % questions.length
        setIndexQuestion(copy_indexQuestion)
        setStat(new_obj())
        setPoint(point_init)
    }
    
    const new_obj = () => {
        let copy_stat = {...stat}
        copy_stat[`area_${questions[indexQuestion].strata}`] += point[`${questions[indexQuestion].strata}`]
        if (copy_stat[`area_${questions[indexQuestion].strata}`] < 0) {
            copy_stat[`area_${questions[indexQuestion].strata}`] = 0
        }
        return copy_stat
    }
    
    const add_point = (pt) => {
        let copy_point = {...point_init}
        let copy_stat = {...stat}
        let current_point = pt * 10000
        for (const key in copy_point) {
            if (key >= questions[indexQuestion].strata) {
                if (copy_stat[`area_${questions[indexQuestion].strata}`] + current_point < 0) {
                    current_point = -copy_stat[`area_${questions[indexQuestion].strata}`]
                    copy_point[key] = current_point
                } else {
                    copy_point[key] = current_point
                }
            }
        }
        setPoint(copy_point)
    }

    const change_color = () => {
        let copy_stat = {...stat}
        copy_stat.area_1 = parseInt(Math.random() * 100000 + 10000) + point['1']
        copy_stat.area_2 = parseInt(Math.random() * 100000 + 10000) + point['2']
        copy_stat.area_3 = parseInt(Math.random() * 100000 + 10000) + point['3']
        copy_stat.area_4 = parseInt(Math.random() * 100000 + 10000) + point['4']
        copy_stat.color_1 = `rgba(${parseInt(Math.random() * 255)}, ${parseInt(Math.random() * 255)}, ${parseInt(Math.random() * 255)})`
        copy_stat.color_2 = `rgba(${parseInt(Math.random() * 255)}, ${parseInt(Math.random() * 255)}, ${parseInt(Math.random() * 255)})`
        copy_stat.color_3 = `rgba(${parseInt(Math.random() * 255)}, ${parseInt(Math.random() * 255)}, ${parseInt(Math.random() * 255)})`
        copy_stat.color_4 = `rgba(${parseInt(Math.random() * 255)}, ${parseInt(Math.random() * 255)}, ${parseInt(Math.random() * 255)})`
        setStat(copy_stat)
    }
    
    return (
        <>
            <button style={{width: '190px', marginBottom: '50px'}} onClick={() => change_color()} >Random avatar</button>
            <Psy_model stat={stat} point={point} is_active={questions[indexQuestion].strata} />
            <div style={{height: '50px'}} ></div>
            <h3>{questions[indexQuestion].question}</h3>
            <div style={{display: 'flex', justifyContent: 'center'}} >
                <p style={{width: '200px', textAlign: 'right', marginRight: '10px'}} >{questions[indexQuestion].label_start}</p>
                <button onClick={() => add_point(-2.5)} >1</button>
                <button onClick={() => add_point(-1.5)} >2</button>
                <button onClick={() => add_point(-.5)} >3</button>
                <button onClick={() => add_point(.5)} >4</button>
                <button onClick={() => add_point(1.5)} >5</button>
                <button onClick={() => add_point(2.5)} >6</button>
                <p style={{width: '200px', textAlign: 'left', marginLeft: '10px'}} >{questions[indexQuestion].label_end}</p>
            </div>
            <button style={{width: '80px', marginBottom: '50px'}} onClick={next_question} >Next</button>
        </>
    )
}

export { Questions }