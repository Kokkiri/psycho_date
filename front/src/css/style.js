const set_style_psy_model = (value) => {
    const style = {
        container: {
            position: 'relative',
            margin: 'auto',
            width: '300px',
            height: '300px'
        },
        div_1: {
            position: 'absolute',
            margin: 'auto',
            borderRadius: '100%',
            width: `${value.area_1}px`,
            height: `${value.area_1}px`,
            backgroundColor: value.color_1,
            left: '50%',
            top: '50%',
            transform: 'translate(-50%, -50%)',
            transition: '.3s'
        },
        div_2: {
            position: 'absolute',
            margin: 'auto',
            borderRadius: '100%',
            width: `${value.area_2}px`,
            height: `${value.area_2}px`,
            backgroundColor: value.color_2,
            left: '50%',
            top: '50%',
            transform: 'translate(-50%, -50%)',
            transition: '.3s'
        },
        div_3: {
            position: 'absolute',
            margin: 'auto',
            borderRadius: '100%',
            width: `${value.area_3}px`,
            height: `${value.area_3}px`,
            backgroundColor: value.color_3,
            left: '50%',
            top: '50%',
            transform: 'translate(-50%, -50%)',
            transition: '.3s'
        },
        div_4: {
            position: 'absolute',
            margin: 'auto',
            borderRadius: '100%',
            width: `${value.area_4}px`,
            height: `${value.area_4}px`,
            backgroundColor: value.color_4,
            left: '50%',
            top: '50%',
            transform: 'translate(-50%, -50%)',
            transition: '.3s'
        }
    }
    return style
}

export { set_style_psy_model }