import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import * as ReactDOMClient from 'react-dom/client'
import './index.css';
import './App.css';
import { Main, Avatars } from './pages'
import reportWebVitals from './reportWebVitals';


const container = document.getElementById('root')
const root = ReactDOMClient.createRoot(container)

root.render(
  <React.StrictMode>
    <div className="App">
      <header className="App-header">
        <Router>
          <Routes>
            <Route path='/' element={<Main />} />
            <Route path='avatars' element={<Avatars />} />
          </Routes>
        </Router>
      </header>
    </div>
  </React.StrictMode>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
