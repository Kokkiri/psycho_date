import { useEffect, useState } from 'react'
import { Banner, Header, Psy_model } from '../components'


const Avatars = () => {
    const [avatars, setAvatars] = useState([])

    useEffect(() => {
        fetch('http://localhost:4000/api/avatars')
        .then(resp => resp.json())
        .then(data => setAvatars(data))
    }, [])

    return (
        <>
            <Header />
            <h2>These are my avatars</h2>
            <ul>{avatars.map(el => <li key={el._id} ><Psy_model stat={el} /></li>)}</ul>
            <Banner />
        </>
    )
}

export { Avatars }