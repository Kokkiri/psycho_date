import { Questions, Banner, Header } from '../components'


const Main = () => {
    return (
        <>
            <Header />
            <h2>Welcome on psycho_date</h2>
            <Questions />
            <Banner />
        </>
    )
}

export { Main }