const area = (val) => {
    let radius = 0
    if (val < 0) {
        val = 0
    }
    radius = Math.sqrt(val / Math.PI)
    return parseInt(radius)
}

export { area }