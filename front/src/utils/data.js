const questions = [
    {
        question: 'Cette question modifie le noyau interne :',
        label_start: 'un peu',
        label_end: 'beaucoup',
        strata: '1'
    },
    {
        question: 'Cette question modifie le noyau externe:',
        label_start: 'pas trop',
        label_end: 'vraiment',
        strata: '2'
    },
    {
        question: 'cette question modifie le manteau :',
        label_start: 'absolument pas',
        label_end: 'Tout à fait',
        strata: '3'
    },
    {
        question: 'Cette question modifie la surface :',
        label_start: 'à peine',
        label_end: 'complètement',
        strata: '4'
    },
]

const stat_init = {
    area_1: 50000,
    area_2: 50000,
    area_3: 50000,
    area_4: 50000,
    color_1: 'rgba(0,0,0, 1)',
    color_2: 'rgba(50,50,50, 1)',
    color_3: 'rgba(100,100,100, 1)',
    color_4: 'rgba(150,150,150, 1)'
}

const point_init = {
    1: 0,
    2: 0,
    3: 0,
    4: 0,
}

export { questions, stat_init, point_init }
