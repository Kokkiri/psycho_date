Si vous vous aviez le choix de n’importe qui dans le monde, qui inviteriez-vous à dîner?
2. Aimeriez-vous être célèbre? De quelle manière?
3. Avant de passer un appel téléphonique, répétez-vous parfois ce que vous allez dire? Pourquoi?
4. A quoi ressemblerait une journée "parfaite" pour vous?
5. Quand avez-vous chanté pour vous-même pour la dernière fois? Pour quelqu’un d’autre?
6. Si vous pouviez vivre jusqu’à 90 ans et garder soit l’esprit soit le corps d’un trentenaire pour les 60 dernières années de votre vie, que choisiriez-vous?
7. Avez-vous un pressentiment secret sur la façon dont vous allez mourir?
8. Citez trois choses que vous et votre partenaire semblez avoir en commun.
9. De quoi vous sentez-vous le plus reconnaissant dans votre vie ?
10. Si vous pouviez changer quelque chose dans la façon dont vous avez été élevé, de quoi s’agirait-il?
11. Prenez quatre minutes et racontez l’histoire de votre vie à votre partenaire, avec le plus de détails possible.
12. Si vous pouviez vous réveiller demain en ayant acquis une qualité ou une compétence, laquelle serait-ce

Set 2 :

13. Si une boule de cristal pouvait vous révéler la vérité sur vous, votre vie, le futur ou quoi que ce soit d’autre, que voudriez-vous savoir?
14. Y a-t-il une chose que vous rêvez de faire depuis longtemps? Pourquoi ne l’avez-vous pas faite?
15. Quel est le plus grand accomplissement de votre vie?
16. Quelle est la chose la plus importante pour vous en amitié?
17. Quel est votre plus beau souvenir?
18. Quel est votre pire souvenir?
19. Si vous saviez que vous alliez mourir soudainement dans un an, changeriez-vous quelque chose à votre façon de vivre? Pourquoi?
20. Que signifie l’amitié pour vous?
21. Quels rôles jouent l’amour et l’affection dans votre vie?
22. Échangez alternativement avec votre partenaire quelque chose que vous considérez être chez lui une caractéristique positive. Partagez-en cinq au total.
23. Dans quelle mesure votre famille est-elle unie et chaleureuse ? Pensez-vous que votre enfance a été plus heureuse que celle de la plupart des gens?
24. Que pensez-vous de votre relation avec votre mère?

Set 3

25. Énoncez chacun trois vérités commençant par le mot "nous". Par exemple, "Nous nous sentons tous les deux dans cette pièce…"
26. Complétez cette phrase : "J’aimerais avoir quelqu’un avec qui partager…"
27.  Si vous deviez devenir un ami proche de votre partenaire, dites-lui ce qui serait important qu’il ou elle sache.
28. Dites à votre partenaire ce que vous aimez chez lui ou chez elle; soyez très honnête cette fois, en disant des choses que vous ne diriez peut-être pas à une personne que vous venez de rencontrer.
29. Partagez avec votre partenaire un moment embarrassant de votre vie.
30. Quand avez-vous pleuré devant quelqu’un pour la dernière fois? Et tout seul?
31. Dites à votre partenaire une chose que vous appréciez déjà chez lui ou chez elle.
32. De quoi ne peut-on pas rire?
33. Si vous deviez mourir ce soir sans la possibilité de communiquer avec qui que ce soit, que regretteriez-vous le plus de n’avoir pas dit à quelqu’un? Pourquoi ne pas le lui avoir dit encore?
34. Votre maison, contenant tout ce qui vous appartient, prend feu. Après avoir sauvé vos proches et vos animaux de compagnie, vous avez le temps d’aller récupérer en toute sécurité une seule chose. Laquelle serait-ce? Pourquoi?
35. Parmi tous les membres de votre famille, la mort de qui vous toucherait le plus? Pourquoi?
36. Partagez un problème personnel et demandez à votre partenaire comment il le gèrerait. Demandez également à votre partenaire de vous dire comment il pense que vous vous sentez par rapport à ce problème.

(37. Regardez votre partenaire dans les yeux en silence pendant quatre minutes.)