var express = require('express')
var path = require('path')
require('dotenv').config()
require('./database/con_db')
const routing = require('./routes')
const cors = require('cors')

var app = express()

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, '../build')))
app.use(routing)

module.exports = app
