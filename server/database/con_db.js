const mongoose = require("mongoose");
const URL = process.env.URL;

mongoose
  .connect(URL)
  .then(() => {
    console.log("connection DB OK")
  })
  .catch((err) => console.log('connection KO', err));
