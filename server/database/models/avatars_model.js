const mongoose = require('mongoose')
const schema = mongoose.Schema

const avatar_schema = schema({
    area_1: Number,
    area_2: Number,
    area_3: Number,
    area_4: Number,
    color_1: String,
    color_2: String,
    color_3: String,
    color_4: String,
})

// avatar_schema.pre('save', function() {
//     console.log('avatars', avatars)
//   return avatars
// })

const Avatars = mongoose.model('avatars', avatar_schema)

module.exports = Avatars