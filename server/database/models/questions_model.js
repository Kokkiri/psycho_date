const mongoose = require('mongoose')
const schema = mongoose.Schema

const questions_schema = schema({
    question: String,
    label_start: String,
    label_end: String,
    strata: String,
})

// questions_schema.pre('save', function() {
//     console.log('avatars', avatars)
//   return avatars
// })

const Questions = mongoose.model('questions', questions_schema)

module.exports = Questions