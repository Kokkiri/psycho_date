const router = require('express').Router()
const avatars = require('./avatars')
const questions = require('./questions')

router.get('/avatars', avatars)
router.get('/questions', questions)

module.exports = router