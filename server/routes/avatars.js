const router = require('express').Router()
const Avatars = require('../database/models/avatars_model')

router.use('/avatars', (req, res) => {
    Avatars.find({})
    .exec()
    .then( avatar => res.json(avatar) )
})

module.exports = router