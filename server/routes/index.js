const router = require('express').Router()
const api = require('./api')

router.use('/api', api)

router.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../build/index.html'))
})

module.exports = router