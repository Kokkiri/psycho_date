const router = require('express').Router()
const Questions = require('../database/models/questions_model')

router.use('/questions', (req, res) => {
    Questions.find({})
    .exec()
    .then( question => res.json(question) )
})

module.exports = router